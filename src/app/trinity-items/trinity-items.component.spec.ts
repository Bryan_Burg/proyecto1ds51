import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrinityItemsComponent } from './trinity-items.component';

describe('TrinityItemsComponent', () => {
  let component: TrinityItemsComponent;
  let fixture: ComponentFixture<TrinityItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrinityItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrinityItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
