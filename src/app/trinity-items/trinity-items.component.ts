import { Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-trinity-items',
  templateUrl: './trinity-items.component.html',
  styleUrls: ['./trinity-items.component.css']
})
export class TrinityItemsComponent implements OnInit {

  @Input()
  itemImg1: string = "https://via.placeholder.com/250C/O";
  @Input()
  itemTitle1: string="lorem";

  @Input()
  itemImg2: string = "https://via.placeholder.com/250C/O";
  @Input()
  itemTitle2: string="lorem";

  @Input()
  itemImg3: string = "https://via.placeholder.com/250C/O";
  @Input()
  itemTitle3: string= "lorem";

  @Input()
  itemImg4: string = "https://via.placeholder.com/250C/O";
  @Input()
  itemTitle4: string="lorem";


  constructor() { }

  ngOnInit(): void {
  }

}
