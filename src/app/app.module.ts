import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PrimeroComponent } from './primero/primero.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TitlesComponent } from './titles/titles.component';
import { HeroComponent } from './hero/hero.component';
import { ParagraphsComponent } from './paragraphs/paragraphs.component';
import { ArticleComponent } from './article/article.component';
import { ItemViewComponent } from './item-view/item-view.component';
import { HeroTopComponent } from './hero-top/hero-top.component';
import { TrinityItemsComponent } from './trinity-items/trinity-items.component';


@NgModule({
  declarations: [
    AppComponent,
    PrimeroComponent,
    HeaderComponent,
    FooterComponent,
    TitlesComponent,
    HeroComponent,
    ParagraphsComponent,
    ArticleComponent,
    ItemViewComponent,
    HeroTopComponent,
    TrinityItemsComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
