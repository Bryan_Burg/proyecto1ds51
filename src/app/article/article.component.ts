import { Component,Input,OnInit } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input()
  title : string = 'Lorem ipsum';
  @Input()
  image : string = 'https://th.bing.com/th/id/R.6cdfaf53cc91efa014108935be9f1214?rik=a93VktZardEMug&riu=http%3a%2f%2fclipart-library.com%2fnew_gallery%2f155644_dogs-png.png&ehk=RNtonwNXU8yJRU6cZFk1Ngt%2fD0YrT5P13fE0NzDV3qQ%3d&risl=1&pid=ImgRaw&r=0';
  @Input()
  paragraph : string = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto doloribus nulla sed rerum eaque rem corporis autem facere pariatur vel ut vitaeipsa ea, labore laborum at nisi quidem vero?';
  @Input()
  link : string = '';

  specs = [

    { 
    "altura" : "80cm",
    "recomedaciones" : "Dar paseos con frecuencia",
    "pesoAdulto" :"20kg ",
    "climaRecomendado" : "seco",
    "pelaje" : "corto",
    "tiempoDeVida" : "12 a 17 años"
    }
  ]
  

  constructor() { }

  ngOnInit(): void {
  }

}
